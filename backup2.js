function insert() {
    id = Date.now();
    var inputValue = document.getElementById("myInput").value;

    if (inputValue === '') {
        alert("You must write something!");
    } else {

        if (typeof localStorage["tasks"] == 'undefined') {
            var task_arr = [];
            var task = {
                'id': id,
                'task': inputValue,
                'date': new Date(),
                'status': false,
            };
            task_arr.push(task);
            localStorage.setItem("tasks", JSON.stringify(task_arr));
            reload();
        } else {
            var store = JSON.parse(localStorage.getItem("tasks"));
            var task = {
                'id': id,
                'task': inputValue,
                'date': new Date(),
                'status': false,
            };
            store.push(task)
            localStorage.setItem("tasks", JSON.stringify(store));
            id = id + 1;
            // window.onload = loadCurrent();
            // self.location.reload();
            reload();
        }
    }
}

function reload() {
    var element = document.getElementById("displayList");
    element.remove();
    var displayList = document.createElement('div')
    displayList.classList.add("displayList");
    displayList.setAttribute("id", "displayList");
    document.getElementById("display_container").appendChild(displayList);
    load();
}

function load() {
    var data = JSON.parse(localStorage.getItem("tasks"));
    var tasksItems = data.map(element => {
        return element.task
    });

    var tasksId = data.map(element => {
        return element.id
    });
    // for(var i=0;i<data.length;i++){
    for (var i = tasksId.length - 1; i >= 0; i--) {
        var task_name = tasksItems[i];
        var id = tasksId[i];

        var container = document.createElement('div');
        document.getElementById("displayList").appendChild(container);
        container.classList.add("container");
        container.setAttribute("id", id);
        container.setAttribute("draggable", true);

        container.addEventListener('dragstart', function (e) {
            ondragstart(event, this);
        });
        container.addEventListener('dragover', function (e) {
            e.preventDefault();
        });

        task_name_div = create_text_function(task_name, id);

        var checkbox_div = document.createElement('div');
        checkbox_div.classList.add("checkbox_div");
        var checkbox = document.createElement("input"); //create new input element
        checkbox.type = "checkbox";
        if (data[i].status) {
            $(checkbox).attr("checked", "true");
            task_name_div.classList.add("task_name_div_strike");
        }
        checkbox.setAttribute("id", "check-" + id);
        checkbox.classList.add("checkbox");
        checkbox.addEventListener('change', strike_text);
        checkbox_div.appendChild(checkbox);
        container.appendChild(checkbox_div);

        container.appendChild(task_name_div)

        close_button_div = create_close_button_function(task_name, id);
        container.appendChild(close_button_div);

    }
    document.getElementById("displayList").addEventListener('drop', function (e) {
        e.preventDefault();
        drop(event, this);
    });
}

function ondragstart(ev, self) {
    ev.dataTransfer.setData("id", ev.target.id);
}

function drop(ev, self) {
    console.log("drop");
    ev.preventDefault();

    target = ev.srcElement;
    if (!target.classList.contains('container'))
        target = target.parentNode;
    currentId = ev.dataTransfer.getData('id');
    item = document.getElementById(currentId);
    // console.log("targerid " + typeof (ev.target.id) + "  source " + typeof (item.id));

    itemIndex = Array.prototype.indexOf.call(item.parentElement.children, item);
    targetIndex = Array.prototype.indexOf.call(target.parentElement.children, target);

    if (targetIndex < itemIndex) {
        var parentDiv = document.getElementById("displayList");
        parentDiv.insertBefore(item, target);
    } else {
        var parentDiv = document.getElementById("displayList");
        parentDiv.insertBefore(item, target.nextSibling);
        document.getElementById('displayList').insertBefore(item, target.nextSibling);
        // ev.target.parentElement.after(document.getElementById(currentId));
    }
    var data = JSON.parse(localStorage.getItem("tasks"));

    let tasksId = data.map(element => {
        return element.id

    });
    let targetIndexInLS;
    let itemIndexInLS;
    for (var i = 0; i < tasksId.length; i++) {
        if (tasksId[i] == (ev.target.id)) {
            targetIndexInLS = i;
        }
        if (tasksId[i] == item.id) {
            itemIndexInLS = i;
        }
    }
    console.log(targetIndexInLS + " source " + itemIndexInLS)
    var data = JSON.parse(localStorage.getItem("tasks"));
    var ar = []
    for (var i = 0; i < data.length; i++) {
        if (parseInt(data[i].id) == parseInt(ev.target.id)) {
            temp = data[i];
            data[i] = data[itemIndexInLS];
            data[itemIndexInLS] = temp;
        }

    }

    localStorage.setItem('tasks', JSON.stringify(data));
    //   relode();
    for (var i = 0; i < data.length; i++) {
        console.log(data[i]);
    }
}

function create_text_function(task_name, id) {
    task_name_div = document.createElement('div');
    var text_id = parseInt(id)
    text_id = text_id + 100;
    task_name_div.setAttribute("id", (text_id));
    task_name_div.classList.add("task_name_div");
    var text_node = document.createTextNode(task_name);
    task_name_div.addEventListener("click", edit_task);
    task_name_div.appendChild(text_node);
    return task_name_div
}

function create_close_button_function(task_name, id) {
    var close_button_div = document.createElement('div');
    var newButton = document.createElement("button"); //create new button
    newButton.classList.add("delete_button");
    newButton.setAttribute("id", "del-" + id);
    var buttonText = document.createTextNode("\u00D7");
    newButton.appendChild(buttonText);
    close_button_div.appendChild(newButton);
    newButton.addEventListener("click", remove_task);
    return close_button_div;
}

function strike_text(e) {
    var ButtonId = e.target.id;
    var divId = ButtonId.split("-")[1];
    var t = parseInt(divId)
    t = t + 100;
    var data = JSON.parse(localStorage.getItem("tasks"));
    for (var i = 0; i < data.length; i++) {
        if (data[i].id === parseInt(divId)) {
            var status_index = i;
        }
    }
    var checkId = "check-" + divId;
    task_name_div = document.getElementById(t);
    checkStatus = document.getElementById(checkId);
    if (checkStatus.checked) {
        console.log("status" + data[status_index].status);
        data[status_index].status = true;
        task_name_div.classList.add("task_name_div_strike");
    } else {
        data[status_index].status = false;
        task_name_div.classList.remove("task_name_div_strike");
    }
    localStorage.setItem('tasks', JSON.stringify(data));
}

function edit_task(e) {
    document.getElementById(e.target.id).contentEditable = true;
    var input = document.getElementById(e.target.id);
    console.log(input);
    var inputid = parseInt(e.target.id);
    inputid = inputid + 100;
    input.setAttribute("id", (inputid));
    input.addEventListener("keydown", function (event) {

        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById(e.target.id).contentEditable = false;
            edit_input((inputid));
        }
    });
    // var div = document.createElement('div');
    // var div = document.getElementById(e.target.id);
    div.appendChild(input);
    var item = document.getElementById(e.target.id);
    item.replaceChild(div, item.childNodes[0]);
}

function blue_function() {
    $("#myInput").blur();
}

function remove_task(e) {
    let ButtonId = e.target.id;
    let divId = ButtonId.split("-")[1];
    let checkId = "check-" + divId;

    checkStatus = document.getElementById(checkId);
    var element = document.getElementById(divId);
    if (checkStatus.checked) {
        store(element, divId);
    } else {
        confirmStatus = confirm("activity is not still done. wanna delete? ");
        store(element, divId);
    }
}

function clear_all() {
    localStorage.removeItem();
    self.location.reload();
}

function store(element, divId) {
    element.remove();

    var data = JSON.parse(localStorage.getItem("tasks"));
    let tasksId = data.map(element => {
        return element.id
    });
    for (var i = 0; i < store.length; i++) { //array update
        const delId = divId;
        if (data[i]["id"] == delId) {
            data.splice(i, 1);
        }
        localStorage.setItem("tasks", JSON.stringify(data));
    }
}

function edit_input(id) {
    var edited_task = document.getElementById(id).innerText;
    i_id = parseInt(id);
    i_id = i_id - 300;
    if (edited_task === '') {
        alert("You must write something!");
    } else {
        var store = JSON.parse(localStorage.getItem("tasks"));
        var task = {
            'id': i_id,
            'task': edited_task,
        };
        for (var i = 0; i < store.length; i++) { //array update
            const delId = i_id;
            if (store[i]["id"] == delId) {
                store.splice(i, 1, task);
            }
            localStorage.setItem("tasks", JSON.stringify(store));
        }
    }
}


//--------------------------------------------------